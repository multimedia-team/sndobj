sndobj (2.6.7+ds1-4) unstable; urgency=medium

  * Team upload.
  * Replace deprecated distutils Python module (Closes: #1065957)

 -- Benjamin Drung <bdrung@debian.org>  Wed, 20 Mar 2024 13:27:03 +0100

sndobj (2.6.7+ds1-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Rename libraries for 64-bit time_t transition.  Closes: #1062907

 -- Benjamin Drung <bdrung@debian.org>  Thu, 29 Feb 2024 16:57:39 +0000

sndobj (2.6.7+ds1-3) unstable; urgency=medium

  * Team upload.

  * B-D on python3-distutils (Closes: #952329)
  * Add salsa-ci configuration
  * Bump standards version to 4.5.0

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Fri, 27 Mar 2020 00:19:43 +0100

sndobj (2.6.7+ds1-2) unstable; urgency=medium

  * Team upload.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Felipe Sateler ]
  * Change maintainer address to debian-multimedia@lists.debian.org

  [ Ondřej Nový ]
  * d/watch: Use https protocol
  * Use debhelper-compat instead of debian/compat

  [ IOhannes m zmölnig (Debian/GNU) ]
  * Add patch for consistent indentation in Python3 code
    (Closes: #947582)
  * Declare that building this package does not require "root" powers
  * Remove trailing whitespace from d/changelog
  * Bump dh compat to 12
  * Bump standards-version to 4.4.1

 -- IOhannes m zmölnig (Debian/GNU) <umlaeute@debian.org>  Wed, 15 Jan 2020 14:20:17 +0100

sndobj (2.6.7+ds1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * debian/patches: Refresh.
  * debian/copyright: Strip VCS files from tarball.
  * debian/contro:
    - Update Vcs-*.
    - Bump Standards-Version.
    - Bump debhelper version to 9.
  * debian/rules: Include global *FLAGS.

 -- Sebastian Ramacher <sramacher@debian.org>  Mon, 28 Nov 2016 21:15:10 +0100

sndobj (2.6.6.1-5.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add patch from Philip Chung to fix compilation with GCC 6. (fixes #811743)

 -- Johannes Brandstätter <jbrandst@2ds.eu>  Sat, 24 Sep 2016 15:30:54 +0200

sndobj (2.6.6.1-5) unstable; urgency=medium

  * Upload to unstable
  * Add upstream license info

 -- Felipe Sateler <fsateler@debian.org>  Thu, 03 Sep 2015 21:04:19 -0300

sndobj (2.6.6.1-4) experimental; urgency=medium

  * Team upload.
  * Rename library package for GCC 5 transition.

 -- Felipe Sateler <fsateler@debian.org>  Fri, 07 Aug 2015 13:16:16 -0300

sndobj (2.6.6.1-3) unstable; urgency=low

  * Build for Multiarch.
  * Replace negated list of architectures with linux-any (Closes: #634493)
  * Properly clean the sources tree.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Sun, 05 Feb 2012 14:28:44 +0100

sndobj (2.6.6.1-2) unstable; urgency=low

  * Add gbp config file.
  * Update my email address.
  * Remove DM-Upload-Allowed: yes
  * Update Standards to 3.9.1.
  * Switch packaging to the 3.0 (quilt) format.
  * Update debian/copyright as per DEP-5 rev.166
  * Skip quilt files in .pc/
  * Unapply patches after git-buildpackage'ing, abort on upstream changes.
  * Remove vcs-control-dir from /usr/include/SndObj.

 -- Alessio Treglia <alessio@debian.org>  Mon, 14 Feb 2011 07:39:31 +0100

sndobj (2.6.6.1-1) unstable; urgency=low

  * New maintainer (Closes: #546965).
  * New upstream release.
  * Switch to debhelper 7, drop cdbs.
  * Add quilt support.
  * debian/control:
    - Don't build-depend on libjack0.100.0-dev (Closes: #527432).
    - Build-depend on scons.
    - Bump Standards.
    - Lines should be shorter than 80 characters.
    - Improve description.
    - Allow uploads by Debian Maintainer.
    - Add Homepage field.
    - Add Vcs-* tags.
  * Update debian/copyright (according to Debian DEP-5 proposal).
  * Bump debian/compat.
  * Drop 02-gcc41.patch patch, refresh the other GCC-related patches.
  * debian/patches/01_shlib.patch:
    - Adjust SConstruct rather than patching Makefile.
  * Remove debian/patches.old directory.
  * Remove debian/docs, the license file is already provided by Debian.
  * Remove debian/libsndobj-dev.files, unnecessary.
  * Don't install static library files.
  * Add watch file.

 -- Alessio Treglia <quadrispro@ubuntu.com>  Fri, 12 Feb 2010 08:42:50 +0100

sndobj (2.6.1a-2.4) unstable; urgency=low

  * Non-maintainer upload
  * patches/01_shlib_fix.patch:
    Fix unresolved symbols by using LFLAGS variable (closes: #519088).
  * control:
    - Added ${misc:Depends} to both packages.
    - Moved libsndobj from devel to libdevel.
    - Renamed ${Source-Version} to ${binary:Version}.
  * patches/04-gcc44.patch: Add #include for g++ 4.4 (closes: #505377)
    (patch from Martin Michlmayr).

 -- Pascal Giard <pascal@debian.org>  Sun, 08 Mar 2009 17:26:09 -0400

sndobj (2.6.1a-2.3) unstable; urgency=low

  * Non-maintainer upload
  * patches/03-gcc43.patch: Add necessary #include directives for g++ 4.3
    (closes: #455174) (patch from Cyril Brulebois)
  * Unset DEB_MAKE_CLEAN_TARGET (closes: #424247)

 -- Ben Hutchings <ben@decadent.org.uk>  Sat, 05 Apr 2008 21:29:16 +0100

sndobj (2.6.1a-2.2) unstable; urgency=low

  * Porter NMU.
  * Don't build depend on libasound2-dev on non-linux systems (closes:
    #361479).

 -- Aurelien Jarno <aurel32@debian.org>  Tue, 15 Jan 2008 02:13:05 +0100

sndobj (2.6.1a-2.1) unstable; urgency=low

  * NMU as part of the GCC 4.1 transition.
  * patches/02-gcc41.patch: Remove extra qualification from C++ header
    files (closes: #356175).

 -- Martin Michlmayr <tbm@cyrius.com>  Sat, 27 May 2006 17:20:25 +0200

sndobj (2.6.1a-2) unstable; urgency=low

  * removed ALSA support for freebsd (closes: #336858)

 -- Guenter Geiger (Debian/GNU) <geiger@debian.org>  Wed,  9 Nov 2005 09:49:15 +0100

sndobj (2.6.1a-1) unstable; urgency=low

  * New upstream release (closes: #275144)
  * C++ transition: renamed libsndobj2 to libsndobj2c2
  * Uses -O2 compilation flag (closes: #282821)
  * Added Jack support

 -- Guenter Geiger (Debian/GNU) <geiger@debian.org>  Fri, 28 Oct 2005 19:43:53 +0200

sndobj (2.5.1-1) unstable; urgency=low

  * New upstream version
  * linked dynamically with fftw
  * Fixed problem when linking with shared library

 -- Guenter Geiger (Debian/GNU) <geiger@debian.org>  Wed,  5 May 2004 14:13:39 +0200

sndobj (2.5-2) unstable; urgency=low

  * shared moved in link command at debian/rules

 -- Guenter Geiger (Debian/GNU) <geiger@debian.org>  Tue, 23 Sep 2003 21:12:31 +0200

sndobj (2.5-1) unstable; urgency=low

  * new upstream release

 -- Guenter Geiger (Debian/GNU) <geiger@debian.org>  Mon, 15 Sep 2003 16:14:32 +0200

sndobj (2.0.3-2) unstable; urgency=low

  * added -fPIC flag to compilation (closes: #198011)

 -- Guenter Geiger (Debian/GNU) <geiger@debian.org>  Thu, 19 Jun 2003 19:28:41 +0200

sndobj (2.0.3-1) unstable; urgency=low

  * New upstream

 -- Guenter Geiger (Debian/GNU) <geiger@debian.org>  Thu, 20 Mar 2003 10:55:47 +0100

sndobj (2.0.2-1) unstable; urgency=low

  * Initial Release.

 -- Guenter Geiger (Debian/GNU) <geiger@debian.org>  Wed,  6 Nov 2002 14:39:51 +0100
