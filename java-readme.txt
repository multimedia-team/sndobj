To build the java module on OSX:

$ scons javamodule=1

this should build a jsndobj.jar and a dynamic module lib_jsndobj.jnilib as
well as lib/libsndobj.dylib. A simple test
is in src/examples/test.java:

$ javac -classpath jsndobj.jar src/examples/test.java
$ java -classpath jsndobj.jar:src/examples test 440

VL



