import JSndObj.*;

public class test {

    public static void main(String[] args) {
        
        HarmTable harm =  new HarmTable(10000,1,1);
        Oscili mod = new Oscili(harm, 4.f, 10.f);
        Oscili osc = new Oscili(harm, 400.f, 10000.f, mod);
        SndRTIO outp = new SndRTIO((short)1, jsndobjConstants.SND_OUTPUT, 1024);
	outp.SetOutput((short)1, osc);
	SndThread thread = new SndThread();

	// thread set-up
        thread.AddObj(mod);
        thread.AddObj(osc);
	thread.AddObj(outp, jsndobjConstants.SNDIO_OUT);
	thread.ProcOn();
        
        long end;          
        if(args[0] != null)
	    end = (long) (Float.parseFloat(args[0])*1000);
        else end = 1000;
        long start = System.currentTimeMillis();
	while(System.currentTimeMillis() < start + end );
            
         thread.ProcOff();
	
    };

};